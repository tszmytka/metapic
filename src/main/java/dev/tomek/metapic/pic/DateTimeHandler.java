package dev.tomek.metapic.pic;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

public class DateTimeHandler {
    private static final DateTimeFormatter FORMATTER_EXIF = DateTimeFormatter.ofPattern("yyyy:MM:dd HH:mm:ss");
    private static final DateTimeFormatter FORMATTER_FILE = DateTimeFormatter.ofPattern("yyyyMMdd_HHmmss");

    private final DateTimeFormatter formatterExifInput;
    private final DateTimeFormatter formatterExifOutput;
    private final DateTimeFormatter formatterFileOutput;

    public DateTimeHandler(ZoneId inputZone, ZoneId outputZone) {
        formatterExifInput = FORMATTER_EXIF.withZone(inputZone);
        formatterExifOutput = FORMATTER_EXIF.withZone(outputZone);
        formatterFileOutput = FORMATTER_FILE.withZone(outputZone);
    }

    public String buildAdjustedExifDateTime(String input) {
        return adjustDateTime(input).format(formatterExifOutput);
    }

    public String buildAdjustedFileName(String input) {
        return adjustDateTime(input).format(formatterFileOutput);
    }

    private ZonedDateTime adjustDateTime(String input) {
        final ZonedDateTime dateTime = ZonedDateTime.parse(input, formatterExifInput);
        return dateTime.plusHours(1);
    }
}
