package dev.tomek.metapic.pic;

import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.SmartLifecycle;
import org.springframework.stereotype.Component;

import java.io.File;
import java.time.Duration;
import java.time.Instant;
import java.time.LocalTime;
import java.time.ZoneId;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicBoolean;

@Component
@RequiredArgsConstructor
public class Adjuster implements SmartLifecycle {
    private static final Logger LOGGER = LoggerFactory.getLogger(Adjuster.class);
    private final AtomicBoolean isRunning = new AtomicBoolean();
    private final FileProcessor fileProcessor;

    @Override
    public void start() {
        isRunning.set(true);
        init();
    }

    @Override
    public void stop() {
        isRunning.set(false);
    }

    @Override
    public boolean isRunning() {
        return isRunning.get();
    }

    private void init() {
        // todo parse and inject command line arguments
        final String baseDirName = "data/tmp/input";
        final ZoneId inputTimezone = ZoneId.of("Europe/Warsaw");
        final ZoneId outputTimezone = ZoneId.of("US/Pacific");

        final Instant t0 = Instant.now();
        final String taskName = Adjuster.class.getSimpleName().toUpperCase();
        LOGGER.info(taskName);
        final File dir = new File(baseDirName);
        if (!dir.isDirectory()) {
            LOGGER.error("No valid directory path provided");
            return;
        }

        final File dirDone = fileProcessor.prepareDirDone(dir);
        LOGGER.info("Starting in directory: " + dir.getName());
        fileProcessor.processFiles(
            Optional.ofNullable(dir.listFiles((dir1, name) -> name.toLowerCase().endsWith(".jpg"))).orElse(new File[]{}),
            dirDone,
            inputTimezone,
            outputTimezone
        );
        LOGGER.info("Finshed " + taskName);
        LOGGER.info("Elapsed time: " + LocalTime.ofSecondOfDay(Duration.between(t0, Instant.now()).getSeconds()));
    }
}
