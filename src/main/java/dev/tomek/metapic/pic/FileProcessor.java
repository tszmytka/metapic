package dev.tomek.metapic.pic;

import org.apache.commons.imaging.Imaging;
import org.apache.commons.imaging.ImagingException;
import org.apache.commons.imaging.common.ImageMetadata;
import org.apache.commons.imaging.formats.jpeg.JpegImageMetadata;
import org.apache.commons.imaging.formats.jpeg.exif.ExifRewriter;
import org.apache.commons.imaging.formats.tiff.TiffField;
import org.apache.commons.imaging.formats.tiff.constants.ExifTagConstants;
import org.apache.commons.imaging.formats.tiff.write.TiffOutputDirectory;
import org.apache.commons.imaging.formats.tiff.write.TiffOutputSet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.ZoneId;
import java.util.HashSet;
import java.util.Set;

@Component
public class FileProcessor {
    private static final Logger LOGGER = LoggerFactory.getLogger(FileProcessor.class);
    private static final String DONE_DIR_SUFFIX = " _metapic_fixed_";
    private final Set<String> usedFilenames = new HashSet<>();


    public File prepareDirDone(File workDir) {
        final File dirDone = new File(workDir.toPath() + DONE_DIR_SUFFIX);
        if (!dirDone.exists() && dirDone.mkdirs()) {
            LOGGER.debug("Created done directory.");
        }
        return dirDone;
    }

    public void processFiles(File[] files, File dirDone, ZoneId inputTimezone, ZoneId outputTimezone) {
        usedFilenames.clear();
        final int filesAmount = files.length;
        LOGGER.info("Found files: " + filesAmount);
        final DateTimeHandler handler = new DateTimeHandler(inputTimezone, outputTimezone);
        for (File file : files) {
            try {
                int i = 0;
                processFile(file, handler, i, filesAmount, dirDone);
            } catch (IOException | ImagingException e) {
                LOGGER.error("Could not process file {}", file.getName(), e);
            }
        }
    }

    private void processFile(File file, DateTimeHandler handler, int i, int filesAmount, File dirDone) throws IOException, ImagingException {
        final String fileName = file.getName();
        LOGGER.info("Processing {} ({}/{})", fileName, ++i, filesAmount);
        if (!fileName.startsWith("IMG_")) {
            Files.copy(file.toPath(), Path.of(dirDone.getPath(), fileName));
            usedFilenames.add(fileName);
            LOGGER.info("Copied to done dir");
            return;
        }
        final ImageMetadata metadata = Imaging.getMetadata(file);
        if (!(metadata instanceof JpegImageMetadata)) {
            throw new IllegalStateException("Metadata not belonging to a Jpeg file.");
        }
        final JpegImageMetadata jpegMetadata = (JpegImageMetadata) metadata;
        final TiffField fieldDate = jpegMetadata.findEXIFValueWithExactMatch(ExifTagConstants.EXIF_TAG_DATE_TIME_ORIGINAL);
        final String inputDate = fieldDate.getValue().toString();
        LOGGER.info("Found date: " + inputDate);
        final TiffOutputSet outputSet = jpegMetadata.getExif().getOutputSet();
        final TiffOutputDirectory exifDir = outputSet.getOrCreateExifDirectory();
        exifDir.removeField(ExifTagConstants.EXIF_TAG_DATE_TIME_ORIGINAL);
        final String outputDate = handler.buildAdjustedExifDateTime(inputDate);
        LOGGER.info("Transformed into: " + outputDate);
        exifDir.add(ExifTagConstants.EXIF_TAG_DATE_TIME_ORIGINAL, outputDate);
        String outputName = handler.buildAdjustedFileName(inputDate);
        while (usedFilenames.contains(outputName)) {
            LOGGER.info("Output name " + outputName + " has already been used. Bumping second.");
            // todo what to do if the date has already been used
//                outputFilename = handler.buildFileName(true);
        }
        try (BufferedOutputStream outputStream = new BufferedOutputStream(new FileOutputStream(buildOutputFile(fileName, dirDone, outputName)))) {
            new ExifRewriter().updateExifMetadataLossless(file, outputStream, outputSet);
            usedFilenames.add(outputName);
            LOGGER.info("Saved file: " + outputName);
        } catch (Exception e) {
            LOGGER.error("Could not store file", e);
        }
        LOGGER.info("Finished processing: " + fileName);
    }

    private File buildOutputFile(String inputFileName, File dirDone, String outputName) {
        String ext = inputFileName.substring(inputFileName.indexOf(".")).toLowerCase();
        return new File(dirDone.getPath() + "/" + outputName + "." + ext);
    }
}
