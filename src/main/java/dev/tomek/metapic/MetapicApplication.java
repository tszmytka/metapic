package dev.tomek.metapic;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MetapicApplication {
	public static void main(String[] args) {
		SpringApplication.run(MetapicApplication.class, args);
	}
}
