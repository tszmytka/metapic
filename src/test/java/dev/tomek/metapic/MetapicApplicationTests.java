package dev.tomek.metapic;

import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import static dev.tomek.metapic.Tags.INTEGRATION_TEST;

@SpringBootTest
@Tag(INTEGRATION_TEST)
class MetapicApplicationTests {

	@Test
	void contextLoads() {
	}

}
