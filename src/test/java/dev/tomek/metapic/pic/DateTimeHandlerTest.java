package dev.tomek.metapic.pic;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.time.ZoneId;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.params.provider.Arguments.arguments;

class DateTimeHandlerTest {

    private DateTimeHandler dateTimeHandler;

    @BeforeEach
    void setUp() {
        dateTimeHandler = new DateTimeHandler(ZoneId.of("Europe/Warsaw"), ZoneId.of("US/Pacific"));
    }

    @ParameterizedTest
    @MethodSource
    void canBuildAdjustedExifDateTime(String expected, String input) {
        assertEquals(expected, dateTimeHandler.buildAdjustedExifDateTime(input));
    }

    private static Stream<Arguments> canBuildAdjustedExifDateTime() {
        return Stream.of(
            arguments("2019:09:14 13:59:32", "2019:09:14 21:59:32"),
            arguments("2019:09:25 22:05:08", "2019:09:26 06:05:08")
        );
    }

    @ParameterizedTest
    @MethodSource
    void canBuildAdjustedFileName(String expected, String input) {
        assertEquals(expected, dateTimeHandler.buildAdjustedFileName(input));
    }

    private static Stream<Arguments> canBuildAdjustedFileName() {
        return Stream.of(
            arguments("20190914_135932", "2019:09:14 21:59:32"),
            arguments("20190925_220508", "2019:09:26 06:05:08")
        );
    }
}